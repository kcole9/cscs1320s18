#include <stdio.h>

int length(char * string);
char charAt(char * string, int position);
int findChar(char * string, char find);
int findNextChar(char * string, char find, int start);
int findString(char * string, char * find);
int findNextString(char * string, char * find, int start);
int replaceChar(char * string, char target, char sub, int count);
int replaceString(char * string, char * target, char * sub, int count);
char * split(char * string, int position);
char ** parse(char * string, char delimiter);

void lower(char * string);
void upper(char * string);
void camel(char * string, int mode);
void unCamel(char * string);
