#include "functions.h"

int findChar(char * string, char find)
{
	int count = 0;
	while(string[count] != '\0')
	{
		if(string[count] == find)
			return count;
		count++;
	}

	return -1;
}
