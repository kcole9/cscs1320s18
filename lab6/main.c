#include "functions.h"

int main()
{

	char * myArrayString[4];
	myArrayString[0] = "This";
	myArrayString[1] = "is";
	myArrayString[2] = "a";
	myArrayString[3] = "sentence";

	for(int i = 0; i < 4; i++)
	{
		printf("my string at %d is %s\n", i, myArrayString[i]);
	}

	printf("The string we are using is: testing\n");
	printf("length: %d\n", length("testing"));
	printf("charAt 4: %c\n", charAt("testing", 4));
	printf("findChar i: %d\n", findChar("testing", 'i'));
	printf("findNextChar t: %d\n", findNextChar("testing", 't', 1));
	printf("findString testing: %d\n", findString("this is a very much long longer testing string", "testing"));
	printf("findNextString testing %d\n", findNextString("testing a testing", "testing", 1));
	printf("replaceChar testing %d\n", replaceChar("testing", 't', 'v', 1));
	printf("splitString testing %s\n", split("testing", 3));
	return 0;
}
