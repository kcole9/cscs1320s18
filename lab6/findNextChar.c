#include "functions.h"

int findNextChar(char * string, char find, int start)
{
	while(string[start] != '\0')
	{
		if(string[start] == find)
			return start;

		start++;
	}
	return -1;
}
