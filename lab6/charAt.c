#include "functions.h"

char charAt(char * string, int position)
{
	if(position < 0 || position > length(string))
		return '\0';

	return string[position];
}
