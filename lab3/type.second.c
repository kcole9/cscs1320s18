#include <stdio.h>
int main( int argc, char *argv[])
{
	char * vd = "void";
	char * chr = "char";
	char * shrt = "short";
	char * integer = "int";
	char * lng = "long";
	char * lnglng = "long long";
	char * flt = "float";
	char * dbl = "double";
	char * sgnd = "signed";
	char * usgnd = "unsigned";
	char * bool = "_Bool";
	char * complex = "_Complex";

	printf("%s%8d\n", vd, sizeof (void) );
	printf("%s%8d\n", chr, sizeof (char) );
	printf("%s%7d\n", shrt, sizeof (short) );
	printf("%s%9d\n", integer, sizeof (int) );
	printf("%s%8d\n", lng, sizeof (long) );
	printf("%s%3d\n", lnglng, sizeof (long long) );
	printf("%s%7d\n", flt, sizeof (float) );
	printf("%s%6d\n", dbl, sizeof (double) );
	printf("%s%6d\n", sgnd, sizeof (signed) );
	printf("%s%4d\n", usgnd, sizeof (unsigned) );
	printf("%s%7d\n", bool, sizeof (_Bool) );
	printf("%s%5d\n", complex, sizeof (_Complex) );
	
	
	return 0;
}

	