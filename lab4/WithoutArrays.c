#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]){
	

int *iter;
int *ones = (int *)malloc( 3 * sizeof (int) );
iter = ones;
*iter++ = 1;
*iter++ = 2;
*iter++ = 3;
iter = ones;
for( int index = 0; index < 3; index++ ) {
    printf( "ones: %p, %d\n", iter, *iter );
    iter++;
}

int *tens = (int *)malloc( 3 * sizeof (int) );
iter = tens;
*iter++ = 10;
*iter++ = 20;
*iter++ = 30;
iter = tens;
for( int index = 0; index < 3; index++ ) {
    printf( "tens: %p, %d\n", iter, *iter );
    iter++;
}

int *hundreds = (int *)malloc( 3 * sizeof (int) );
iter = hundreds;
*iter++ = 100;
*iter++ = 200;
*iter++ = 300;
iter = hundreds;
for( int index = 0; index < 3; index++ ) {
    printf( "hundreds: %p, %d\n", iter, *iter );
    iter++;
}

}