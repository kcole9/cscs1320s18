#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>

using namespace std;

int main( ) { 
    int integer;
    double real;
    string word;

    cout << "enter an integer: ";
    cin >> integer;
    cout << "integer: " << setw(20) << setfill('1') << integer << endl;

    cout << "enter a real number:  ";
    cin >> real;
    cout << "real: " << setw(56) << setfill('7') << left << real << setprecision(5) << endl;

    cout << "enter a word: ";
    cin >> word;
    cout << "word: " << uppercase << word << endl;
	
	ifstream inFile( "sample.data" );
    if( !inFile.is_open( ) ) { cout << "file open error" << endl; return 1; }
    string dat;

    inFile >> integer;
    cout << "integer: " << integer << endl;

    inFile >> real;
    cout << "real: " << real << endl;

    inFile >> word;
    cout << "word: " << word << endl;

    inFile.close( );
	
	ofstream outFile( "sample.out" );
    if( !outFile.is_open( ) ) { cout << "file open error" << endl; return 2; }
    outFile << integer << "," << real << "," << word << endl;

    return 0;
}