#include <stdio.h>
#include <stdlib.h>
#include <string.h>


struct item {
   char * value;
   struct item * next;
   struct item * previous;
};

int main( int argc, char *argv[ ] ) { 
   char * string;
   string = "rice";
   struct item a;
   a.value = string;
   a.next = NULL;
   a.previous = NULL;

   string = "spinach";
   struct item b;
   b.value = string;
   b.next = NULL;
   b.previous = &a;
   a.next = &b;

   string = "salmon";
   struct item c;
   c.value = string;
   c.next = NULL;
   c.previous = &b;
   b.next = &c;

   string = "cheese";
   struct item d;
   d.value = string;
   d.next = NULL;
   d.previous = &c;
   c.next = &d;
   
   struct item * list;
   list = &a;
   printf( "%s\n", (*list).value );
   printf( "%s\n", list->next->value );

   while(list->next != NULL)
   {
   printf( "%s\n", list->value );
   list = list->next;
   
   }
   printf( "%s\n", list->value );

   return 0;
}