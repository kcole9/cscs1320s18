#include <stdio.h>

#define check -1

void help();
void usage();
int add(int x, int y);
int subtract(int x, int y);
int multiply(int x, int y);
int divide(int x, int y);
int validate(char *args[]);


int main(int argc, char *argv[]) 
{
	printf("\n");
	
	//exit if args are less than 4
	if(argc < 4)
	{
		usage();
		return 0; //exits early
	}	
	
	//exit if arg 3 is not one of + - * /
	if(*argv[3] != '+' && *argv[3] != '-' && *argv[3] != '*' && *argv[3] != '/') 
	{
			printf("argv[3] must be either +-*/");
			return 0; //exits early
	}	
	
	//finish validate function
	if(validate(argv) == check)
		//return 0; //exit early
	
	//do Joe stuff
	for(int count = 0; count < argc; count++)
	{
      printf("arg %d: %s\n", count, argv[count]);
	  
      if(strcmp(argv[count], "help") == 0) help();
      
	  //what does that [0] after [count] do????
	  if(*argv[count] >= 'a' && argv[count][0] <= 'z')
         printf("First characterer is lowercase letter.\n");
	  
	  printf("Integer value is: %d\n\n", atoi(argv[count]));  
	}
   
	//Do specific operation
	if(*argv[3] == '+')
		printf("%d\n", add(atoi(argv[2]), atoi(argv[4])));
	
	if(*argv[3] == '-')
		printf("%d\n", subtract(atoi(argv[2]), atoi(argv[4])));
	
	if(*argv[3] == '*')
		printf("%d\n", multiply(atoi(argv[2]), atoi(argv[4])));
	
	if(*argv[3] == '/')
		printf("%d\n", divide(atoi(argv[2]), atoi(argv[4])));
	
	return 0;
} 

void help() 
{
   printf( "  Help is on its way...\n" );
}

int add(int x, int y)
{
	return x + y;
}

int subtract(int x, int y)
{
	return x - y;
}

int multiply(int x, int y)
{
	return x * y;
}

int divide(int x, int y)
{
	if (y == 0)
	{
		printf("Cannot divide by 0\n");
		return 0;
	}
	
	return x / y;
}

int validate(char *args[])
{
	int value = 0;
	//if( argv2 and argv4 are not a number)
	//{
		//value = -1;
		//printf("Please enter valid numbers into args 2 & 4");
	//}
	
	return value;
}

void usage()
{
	printf("usage: arg1[char *] arg2 [int] arg3[+-*/] arg4[int] arg5[char *]");
}

//function to handle 2nd and 4th arguments do not start with a number 
//3. display message if second or fourth arguments do not start with a number



