#include <stdio.h>
#include <stdlib.h>


int main( int argc, char *argv[])
{
	FILE * fil = fopen("lab7.txt", "r");
	fseek( fil, 0, SEEK_END );
	printf("%ld\n", ftell( fil ) );
	char * string = (char *)malloc( ftell( fil ) + 1);
	
	fseek(fil, 0, SEEK_SET );
	while( ( *string = fgetc( fil )) != EOF) 
	{
		printf( "%c", *string );
		string++;
		
	}
	*string = '\0';
	printf("\n%s\n", string );
	return 0;
	
}