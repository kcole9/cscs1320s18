#include <iostream>
#include <iomanip>
#include <string>
#include <vector>

using namespace std;

class Guy
{
	private:
	vector<string> inventory;
	vector<string> used;
	
	public:
	Guy(){}
	void usePart();
	bool complete();
	
};

class Player1
{
	private:
	string secret;
	string correct;
	string incorrect;
	string status;
	Guy *guy;

	public:
	Player1(){}
	void setSecret(string theSecret);
	bool checkGuess(string aLetter);
	
};

class Player2
{
	private:
	Player1 *player1;
	
	public:
	Player2(Player1 & player1){}
	
	bool guess(string aLetter);	
};


int main()
{
	return 0;
}
