#include <stdio.h>
#include <iostream>
#include <iomanip>

using std::cout;
using namespace std;

int main( ){
	
	printf( "Hello, printf\n");
	cout << "Hello, from cout; pi;" << setw(15) << left << hex << 123 << 3.14 << endl;
	COUT << 80 << endl;
	
	bool sunny = false;
	cout << "sunny:" << sunny << endl;
	cout << boolalpha;
	cout << "sunny:" << sunny << endl;
	cout << noboolalpha;
	cout << "sunny:" << sunny << endl;
	
	return 0;
}