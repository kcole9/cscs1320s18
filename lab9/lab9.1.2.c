#include <SDL/SDL.h>
#include <math.h>

int main( int argc, char *argv[] ) {
   SDL_Surface *screen;
   atexit( SDL_Quit );
   if( SDL_Init( SDL_INIT_VIDEO ) < 0 ) exit( 1 );
   SDL_WM_SetCaption( "Data Analysis", NULL );
   screen = SDL_SetVideoMode( 640 , 600 , 32 , SDL_DOUBLEBUF|SDL_HWSURFACE|SDL_ANYFORMAT );
   SDL_FillRect( screen , NULL , 0x00FFFF);

   SDL_LockSurface( screen );
   int bpp = screen->format->BytesPerPixel;
   Uint32 red = SDL_MapRGB( screen->format, 0xFF, 0x99, 0x31);

   Uint8 *p;
   int data[ 8 ][ 2 ] = { 10, 20, 50 ,40, 100, 30, 150, 50, 200, 55 };
   double x, x0, x1, y, y0, y1, length, deltax, deltay;   
   for( int count = 0; count < 4; count++ ) {
      x0 = data[ count ][ 0 ];
      y0 = data[ count ][ 1 ];
      x1 = data[ count + 1 ][ 0 ];
      y1 = data[ count + 1 ][ 1 ];
      x = x1 - x0;
      y = y1 - y0;
      length = sqrt( x*x + y*y );
      deltax = x / length;
      deltay = y / length;
      x = x0;
      y = y0;
      for( int i = 0; i < length; i++ ) {
         p = (Uint8 *)screen->pixels + (int)y * screen->pitch + (int)x * bpp;
         *(Uint32 *)p = red;
         x += deltax;
         y += deltay;
      }
   }
   SDL_UnlockSurface(screen);

   SDL_Flip(screen);

   SDL_Event event;
   while( 1 ) {
      SDL_PollEvent( &event );
      if( event.type == SDL_KEYDOWN )
         if( event.key.keysym.sym == SDLK_DOWN ) break;
   }
   return 0;
}