#include <SDL/SDL.h>
#include <math.h>

SDL_Surface *screen;
Uint32 red;
Uint32 white;
int bpp;

void sdlInit()
{
	atexit(SDL_Quit);
	if(SDL_Init(SDL_INIT_VIDEO) < 0) exit(1);
	SDL_WM_SetCaption("Data Analysis", NULL);
	screen = SDL_SetVideoMode( 600, 600, 32, SDL_DOUBLEBUF|SDL_HWSURFACE|SDL_ANYFORMAT);
	SDL_FillRect(screen, NULL, 0x000000);

	SDL_LockSurface(screen);
	bpp = screen->format->BytesPerPixel;
	red = SDL_MapRGB(screen->format, 0xFF, 0x99, 0x31);
	white = SDL_MapRGB(screen->format, 0xFF, 0xFF, 0xFF);
}

void dataPlotting()
{
	
   Uint8 *p;
   int data[ 8 ][ 2 ] = { 10, 20, 50 ,40, 100, 30, 150, 50, 200, 55 };
   double x, x0, x1, y, y0, y1, length, deltax, deltay;   
   for( int count = 0; count < 4; count++ ) {
      x0 = data[ count ][ 0 ];
      y0 = data[ count ][ 1 ];
      x1 = data[ count + 1 ][ 0 ];
      y1 = data[ count + 1 ][ 1 ];
      x = x1 - x0;
      y = y1 - y0;
      length = sqrt( x*x + y*y );
      deltax = x / length;
      deltay = y / length;
      x = x0;
      y = y0;
      for( int i = 0; i < length; i++ ) {
         p = (Uint8 *)screen->pixels + (int)(540 - y) * screen->pitch + (int)(x + 50) * bpp;
         *(Uint32 *)p = red;
         x += deltax;
         y += deltay;
      }

   }
}

void drawAxes()
{
	Uint8 *point;

	//Draw X axis
	for(int i = 50; i < 640; i++)
	{
       point = (Uint8 *)screen->pixels + (int)540 * screen->pitch + (int)i * bpp;
	   *(Uint32*)point = white;
	
	   if(i % 50 == 0)
	   {
	   		for(int j = 540; j < 555; j++)
			{
				point = (Uint8*)screen->pixels + (int)j * screen->pitch + (int)i * bpp;
				*(Uint32*)point = white;
			}
	   }
	}

	//Draw Y axis
	for(int i = 0; i <= 540; i++)
	{
       point = (Uint8 *)screen->pixels + (int)i * screen->pitch + (int)50 * bpp;
	   *(Uint32*)point = white;

	   if(i % 10 == 0)
	   {
			for(int j = 50; j > 35; j--)
			{
				point = (Uint8*)screen->pixels + (int)i * screen->pitch + (int)j *bpp;
				*(Uint32*)point = white;
			}
	   }		
	}
}

void eventHandling()
{
   SDL_UnlockSurface(screen);

   SDL_Flip(screen);

   SDL_Event event;
   while( 1 ) {
      SDL_PollEvent( &event );
      if( event.type == SDL_KEYDOWN )
         if( event.key.keysym.sym == SDLK_ESCAPE ) break;
   }

}


int main( int argc, char *argv[] )
{
   sdlInit();
   drawAxes();
   dataPlotting();
   eventHandling();
   return 0;
}
