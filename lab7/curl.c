#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>

void getHead(char * address);
void getBody(char * address);

int main(int argc, char * argv[])
{
	if(argc < 2)
		printf("must enter a web address");

	char * address = argv[1];

	getHead(address);
	getBody(address);
    return 0;
}

void getHead(char * address)
{
	char * cmd;
	asprintf(&cmd, "curl %s | grep 'head' > head.txt", address);
	system(cmd);
}

void getBody(char * address)
{
	char * cmd;
	asprintf(&cmd, "curl %s | grep 'body' > body.txt", address);
	system(cmd);
}



